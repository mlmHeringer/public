/*
 * Copyright (c) 2014, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
// Standard C Included Files
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// SDK Included Files
#include "fsl_tpm_driver.h"
#include "board.h"
#include "fsl_debug_console.h"
#include "fsl_os_abstraction.h"
#include "fsl_rtc_driver.h"

// Benchmark include
#include <stddef.h>
#include <string.h>
#include <limits.h>

// LCD Include files
#include "lcd_hal.h"

static size_t table[UCHAR_MAX + 1];
static size_t len;
static char *findme;

#define TMR_PERIOD         500000U
#if defined(TWR_KV46F150M)
#define LPTMR0_IDX LPTMR_IDX
#endif
#define TPM_MAX_MOD 65536
#define RTC_INSTANCE      BOARD_RTC_FUNC_INSTANCE

int OF_COUNT = 0; //OverFlow COUNTer
uint32_t TPM0Clock, TPM1Clock, CoreClock;

/*!
* @brief ISR for Alarm interrupt
*
* This function change state of busyWait.
*/
void RTC_IRQHandler(void)
{
    if (RTC_DRV_IsAlarmPending(RTC_INSTANCE))
    {
        //busyWait = false;
        // Disable second interrupt
        RTC_DRV_SetAlarmIntCmd(RTC_INSTANCE, false);
    }
}
/*!
 * @brief Interrupt handler
 *
 * This function is used to handle interruptions
 * and increment a counter
 */
void TPM0_IRQHandler()
{
    //Clear the overflow mask if set.   According to the reference manual, we clear by writing a logic one!
	if(TPM0_SC & TPM_SC_TOF_MASK) // Este IF nao esta fazendo sentido
	{
		TPM0_SC |= TPM_SC_TOF_MASK;
	}
	OF_COUNT = OF_COUNT + 1;
}

void init_board(int tpmMod)
{

	tpm_general_config_t driverInfo;
	/*tpm_pwm_param_t param = {
			.mode              = kTpmEdgeAlignedPWM,
			.edgeMode          = kTpmHighTrue,
			.uFrequencyHZ      = 120000u,
			.uDutyCyclePercent = 99u
	};*/

	// Init hardware
	hardware_init();

	// Inicializacao LCD para release
	lcd_initLcd();

	// Print a note.
	printf("Hardware inicializado\r\n");
	// Prepare memory for initialization.
	memset(&driverInfo, 0, sizeof(driverInfo));
	// Init TPM.
	TPM_DRV_Init(BOARD_TPM_INSTANCE, &driverInfo);
	// Set clock for TPM.
	TPM_DRV_SetClock(BOARD_TPM_INSTANCE, kTpmClockSourceModuleHighFreq, kTpmDividedBy2);

	TPM0Clock = TPM_DRV_GetClock(BOARD_TPM_INSTANCE);
	CoreClock = CLOCK_SYS_GetSystemClockFreq();
	printf("TPM configurado\r\n");
	//printf("Core Clock: %d || TPM 0 Clock: %d || TPM Mod: %d \r\n", CoreClock, TPM0Clock, tpmMod);
}


/*!
 * @brief Write OF count values to release
 *
 *
 */
void writeOF(int OFMedido)
{
	char cOverflow[10];
	int i, div;
	div = OFMedido;
	for(i = 9; i >= 0; i--)
	{
		cOverflow[i] = div % 10 + '0';
		div =  div/10;
	}

	lcd_sendCommand(CMD_CLEAR);
	lcd_writeString(cOverflow);
	lcd_setCursor(1,0);

}
/*
**  Call this with the string to locate to initialize the table
*/

void init_search(const char *string)
{
      size_t i;

      len = strlen(string);
      for (i = 0; i <= UCHAR_MAX; i++)                      /* rdg 10/93 */
            table[i] = len;
      for (i = 0; i < len; i++)
            table[(unsigned char)string[i]] = len - i - 1;
      findme = (char *)string;
}

char *strsearch(const char *string)
{
      register size_t shift;
      register size_t pos = len - 1;
      char *here;
      size_t limit=strlen(string);

      while (pos < limit)
      {
            while( pos < limit &&
                  (shift = table[(unsigned char)string[pos]]) > 0)
            {
                  pos += shift;
            }
            if (0 == shift)
            {
                  if (0 == strncmp(findme,
                        here = (char *)&string[pos-len+1], len))
                  {
                        return(here);
                  }
                  else  pos++;
            }
      }
      return NULL;
}

/*!
 * @brief Use FTM in PWM mode
 *
 * This function use PWM to control brightness of a LED.
 * LED is brighter and then dimmer, continuously.
 */
int main(void)
{
	rtc_datetime_t date, date_begin;
	int tpmMod;
	tpmMod = TPM_MAX_MOD/2;
	LED1_EN;
	LED2_EN;
	LED2_ON;
	init_board(tpmMod);
    // Init RTC
    RTC_DRV_Init(RTC_INSTANCE);
    // Set a start date time and start RT.
    date.year   = 2016U;
    date.month  = 7U;
    date.day    = 31U;
    date.hour   = 16U;
    date.minute = 20U;
    date.second = 0U;
    date_begin = date;
	TPM_DRV_CounterStart (BOARD_TPM_INSTANCE, kTpmCountingUp, tpmMod, 1);

	// Inicio benchmark
    char *here;
    char *find_strings[] = {"abb",  "you", "not", "it", "dad", "yoo", "hoo",
                            "oo", "oh", "xx", "xx", "x", "x", "field", "new", "row",
			      "regime", "boom", "that", "impact", "and", "zoom", "texture",
			      "magnet", "doom", "loom", "freq", "current", "phase",
			      "images",
			      "appears", "phase", "conductor", "wavez",
			      "normal", "free", "termed",
			      "provide", "for", "and", "struct", "about", "have",
			      "proper",
			      "involve",
			      "describedly",
			      "thats",
			      "spaces",
			      "circumstance",
			      "the",
			      "member",
			      "such",
			      "guide",
			      "regard",
			      "officers",
			      "implement",
			      "principalities",
			      NULL};
    char *search_strings[] = {"cabbie", "your", "It isn't here",
                              "But it is here", "hodad", "yoohoo", "yoohoo",
                              "yoohoo", "yoohoo", "yoohoo", "xx", "x", ".",
				"In recent years, the field of photonic ",
				"crystals has found new",
				"applications in the RF and microwave",
				"regime. A new type of metallic",
				"electromagnetic crystal has been",
				"developed that is having a",
				"significant impact on the field of",
				"antennas. It consists of a",
				"conductive surface, covered with a",
				"special texture which alters its",
				"electromagnetic properties. Made of solid",
				"metal, the structure",
				"conducts DC currents, but over a",
				"particular frequency range it does",
				"not conduct AC currents. It does not",
				"reverse the phase of reflected",
				"waves, and the effective image currents",

				"appear in-phase, rather than",
				"out-of-phase as they are on normal",
				"conductors. Furthermore, surface",
				"waves do not propagate, and instead",
				"radiate efficiently into free",
				"space. This new material, termed a",
				"high-impedance surface, provides",
				"a useful new ground plane for novel",
				"low-profile antennas and other",
				"electromagnetic structures.",
				"The recent protests about the Michigamua",
				"student organization have raised an",
				"important question as to the proper nature",
				"and scope of University involvement",
				"with student organizations. Accordingly",
				"the panel described in my Statement of",
				"February 25, 2000 that is considering the",
				"question of privileged space also will",
				"consider under what circumstances and in",
				"what ways the University, its",
				"administrators and faculty members should",
				"be associated with such organizations",
				"and it will recommend guiding principles",
				"in this regard. The University's",
				"Executive Officers and I will then decide",
				"whether and how to implement such",
				"principles."
};
    int i;

    for (i = 0; find_strings[i]; i++)
    {
          init_search(find_strings[i]);
          here = strsearch(search_strings[i]);
          printf("\"%s\" is%s in \"%s\"", find_strings[i],
                here ? "" : " not", search_strings[i]);
          if (here)
                printf(" [\"%s\"]\n", here);
          //putchar('\n');
    }
	//Benchmark fim

    LED2_OFF;
    LED1_ON;
    int OFMedido = OF_COUNT;
    printf("Overflows: %d \r\n", OF_COUNT);
        RTC_DRV_GetDatetime(RTC_INSTANCE, &date);
        PRINTF("Current datetime: %04hd-%02hd-%02hd %02hd:%02hd:%02hd\r\n",
                		date.year, date.month, date.day, date.hour, date.minute, date.second);
        date_begin.year =  date.year - date_begin.year;
        date_begin.month =  date.month - date_begin.month;
        date_begin.day =  date.day - date_begin.day;
        date_begin.hour =  date.hour - date_begin.hour;
        date_begin.minute =  date.minute - date_begin.minute;
        date_begin.second =  date.second - date_begin.second;

        PRINTF("Current datetime: %04hd-%02hd-%02hd %02hd:%02hd:%02hd\r\n",
        		date_begin.year, date_begin.month, date_begin.day, date_begin.hour, date_begin.minute, date_begin.second);

    //Write results to LCD
    //writeOF(OFMedido);
    return 0;
}
///////////////////////////////////////////////////////////////////////////////
// EOF
///////////////////////////////////////////////////////////////////////////////

