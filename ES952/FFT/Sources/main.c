/*
 * Programa: MiBench - FFT
 * Autor: Marcos Heringer
 * Data: 22/08/16
 * Comentário: Rodar o benchmark FFT no modo Debug/Release
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "fsl_tpm_driver.h"
#include "board.h"
#include "fsl_debug_console.h"
#include "fsl_os_abstraction.h"
#include "fsl_rtc_driver.h"
#include "fsl_i2c_master_driver.h"

// LCD Include files
#include "lcd_hal.h"

#define TMR_PERIOD         500000U
#if defined(TWR_KV46F150M)
#define LPTMR0_IDX LPTMR_IDX
#endif
#define TPM_MAX_MOD 65536
#define RTC_INSTANCE      BOARD_RTC_FUNC_INSTANCE
#define LCD_INSTANCE 0

int OF_COUNT = 0; //OverFlow COUNTer
uint32_t TPM0Clock, TPM1Clock, CoreClock;

/*!
 * @brief ISR for Alarm interrupt
 *
 * This function change state of busyWait.
 */
void RTC_IRQHandler(void)
{
	if (RTC_DRV_IsAlarmPending(RTC_INSTANCE))
	{
		//busyWait = false;
		// Disable second interrupt
		RTC_DRV_SetAlarmIntCmd(RTC_INSTANCE, false);
	}
}
/*!
 * @brief Interrupt handler
 *
 * This function is used to handle interruptions
 * and increment a counter
 */
void TPM0_IRQHandler()
{
	//Clear the overflow mask if set.   According to the reference manual, we clear by writing a logic one!
	if(TPM0_SC & TPM_SC_TOF_MASK) // Este IF nao esta fazendo sentido
	{
		TPM0_SC |= TPM_SC_TOF_MASK;
	}
	OF_COUNT = OF_COUNT + 1;
}

void init_board(int tpmMod)
{

	volatile uint32_t cnt = 0;
	int32_t step = 1;
	tpm_general_config_t driverInfo;
	tpm_pwm_param_t param = {
			.mode              = kTpmEdgeAlignedPWM,
			.edgeMode          = kTpmHighTrue,
			.uFrequencyHZ      = 120000u,
			.uDutyCyclePercent = 99u
	};

	// Init hardware
	hardware_init();

	// Inicializacao LCD para release
	lcd_initLcd();

	// Print a note.
	printf("Hardware inicializado\r\n");
	// Prepare memory for initialization.
	memset(&driverInfo, 0, sizeof(driverInfo));
	// Init TPM.
	TPM_DRV_Init(BOARD_TPM_INSTANCE, &driverInfo);
	// Set clock for TPM.
	TPM_DRV_SetClock(BOARD_TPM_INSTANCE, kTpmClockSourceModuleHighFreq, kTpmDividedBy128);

	TPM0Clock = TPM_DRV_GetClock(BOARD_TPM_INSTANCE);
	CoreClock = CLOCK_SYS_GetSystemClockFreq();
	printf("TPM configurado\r\n");
	printf("Core Clock: %d || TPM 0 Clock: %d || TPM Mod: %d \r\n", CoreClock, TPM0Clock, tpmMod);
}

void writeOF(int OFMedido)
{
	char cOverflow[10];
	int i = 0, resto, div;
	div = OFMedido;
	for(i = 9; i >= 0; i--)
	{
		cOverflow[i] = div % 10 + '0';
		div =  div/10;
	}

	lcd_sendCommand(CMD_CLEAR);
	lcd_writeString(cOverflow);
	lcd_setCursor(1,0);

}

int main(int argc, char *argv[]) {
	unsigned MAXSIZE;
	unsigned MAXWAVES;
	unsigned i,j;
	float *RealIn;
	float *ImagIn;
	float *RealOut;
	float *ImagOut;
	float *coeff;
	float *amp;
	int invfft=0;

	rtc_datetime_t date, date_begin;
	int tpmMod = TPM_MAX_MOD/16;
	init_board(tpmMod);
	LED1_EN;
	LED2_EN;
	LED2_ON;
	// Init RTC
	RTC_DRV_Init(RTC_INSTANCE);
	// Set a start date time and start RT.
	date.year   = 2016U;
	date.month  = 7U;
	date.day    = 31U;
	date.hour   = 16U;
	date.minute = 20U;
	date.second = 0U;

	date_begin = date;

	// Set RTC time to default
	RTC_DRV_SetDatetime(RTC_INSTANCE, &date);

	TPM_DRV_CounterStart(BOARD_TPM_INSTANCE, kTpmCountingUp, tpmMod, 0);
	/*if (argc<3)
	{
		PRINTF("Usage: fft <waves> <length> -i\n");
		PRINTF("-i performs an inverse fft\n");
		PRINTF("make <waves> random sinusoids");
		PRINTF("<length> is the number of samples\n");
		exit(-1);
	}
	else if (argc==4)
		invfft = !strncmp(argv[3],"-i",2);

	MAXSIZE=atoi(argv[2]);
	MAXWAVES=atoi(argv[1]);*/

	//Inicio benchmark
	// Ajustar valores manualmente
	MAXSIZE=16;
	MAXWAVES=10;

	srand(1);

	RealIn=(float*)malloc(sizeof(float)*MAXSIZE);
	ImagIn=(float*)malloc(sizeof(float)*MAXSIZE);
	RealOut=(float*)malloc(sizeof(float)*MAXSIZE);
	ImagOut=(float*)malloc(sizeof(float)*MAXSIZE);
	coeff=(float*)malloc(sizeof(float)*MAXWAVES);
	amp=(float*)malloc(sizeof(float)*MAXWAVES);

	/* Makes MAXWAVES waves of random amplitude and period */
	for(i=0;i<MAXWAVES;i++) 
	{
		coeff[i] = rand()%1000;
		amp[i] = rand()%1000;
	}
	for(i=0;i<MAXSIZE;i++)
	{
		/*   RealIn[i]=rand();*/
		RealIn[i]=0;
		for(j=0;j<MAXWAVES;j++)
		{
			/* randomly select sin or cos */
			if (rand()%2)
			{
				RealIn[i]+=coeff[j]*cos(amp[j]*i);
			}
			else
			{
				RealIn[i]+=coeff[j]*sin(amp[j]*i);
			}
			ImagIn[i]=0;
		}
	}

	/* regular*/
	fft_float (MAXSIZE,invfft,RealIn,ImagIn,RealOut,ImagOut);

	PRINTF("RealOut:\n");
	for (i=0;i<MAXSIZE;i++)
		PRINTF("%f \t", RealOut[i]);
	PRINTF("\n");

	PRINTF("ImagOut:\n");
	for (i=0;i<MAXSIZE;i++)
		PRINTF("%f \t", ImagOut[i]);
	PRINTF("\n");

	free(RealIn);
	free(ImagIn);
	free(RealOut);
	free(ImagOut);
	free(coeff);
	free(amp);


	// Fim Benchmark
	LED2_OFF;
	LED1_ON;
    int OFMedido = OF_COUNT;
    printf("Overflows: %d \r\n", OF_COUNT);
	RTC_DRV_GetDatetime(RTC_INSTANCE, &date);
	PRINTF("Current datetime: %04hd-%02hd-%02hd %02hd:%02hd:%02hd\r\n",
			date.year, date.month, date.day, date.hour, date.minute, date.second);
	date_begin.year =  date.year - date_begin.year;
	date_begin.month =  date.month - date_begin.month;
	date_begin.day =  date.day - date_begin.day;
	date_begin.hour =  date.hour - date_begin.hour;
	date_begin.minute =  date.minute - date_begin.minute;
	date_begin.second =  date.second - date_begin.second;

	PRINTF("Current datetime: %04hd-%02hd-%02hd %02hd:%02hd:%02hd\r\n",
			date_begin.year, date_begin.month, date_begin.day, date_begin.hour, date_begin.minute, date_begin.second);

    //Write results to LCD
    //writeOF(OFMedido);

	exit(0);
}
