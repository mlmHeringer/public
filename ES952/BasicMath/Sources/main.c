/*
 * Programa: MiBench - Small Math
 * Autor: Marcos Heringer
 * Data: 22/08/16
 * Comentário: Rodar o benchmark Small Math no modo Debug/Release
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
// Standard C Included Files
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// SDK Included Files
#include "fsl_tpm_driver.h"
#include "board.h"
#include "fsl_debug_console.h"
#include "fsl_os_abstraction.h"
#include "fsl_rtc_driver.h"

// LCD Include files
#include "lcd_hal.h"

#define TMR_PERIOD         500000U
#if defined(TWR_KV46F150M)
#define LPTMR0_IDX LPTMR_IDX
#endif
#define TPM_MAX_MOD 65536
#define RTC_INSTANCE      BOARD_RTC_FUNC_INSTANCE

#include "snipmath.h"
#include <math.h>

int OF_COUNT = 0; //OverFlow COUNTer
uint32_t TPM0Clock, TPM1Clock, CoreClock;

/*!
* @brief ISR for Alarm interrupt
*
* This function change state of busyWait.
*/
void RTC_IRQHandler(void)
{
    if (RTC_DRV_IsAlarmPending(RTC_INSTANCE))
    {
        //busyWait = false;
        // Disable second interrupt
        RTC_DRV_SetAlarmIntCmd(RTC_INSTANCE, false);
    }
}
/*!
 * @brief Interrupt handler
 *
 * This function is used to handle interruptions
 * and increment a counter
 */
void TPM0_IRQHandler()
{
    //Clear the overflow mask if set.   According to the reference manual, we clear by writing a logic one!
	if(TPM0_SC & TPM_SC_TOF_MASK) // Este IF nao esta fazendo sentido
	{
		TPM0_SC |= TPM_SC_TOF_MASK;
	}
	OF_COUNT = OF_COUNT + 1;
}

void init_board()
{

	volatile uint32_t cnt = 0;
	int32_t step = 1;
	tpm_general_config_t driverInfo;
	tpm_pwm_param_t param = {
			.mode              = kTpmEdgeAlignedPWM,
			.edgeMode          = kTpmHighTrue,
			.uFrequencyHZ      = 120000u,
			.uDutyCyclePercent = 99u
	};

	// Init hardware
	hardware_init();
	// Inicializacao LCD para release
	lcd_initLcd();
	// Print a note.
	printf("Hardware inicializado\r\n");
	// Prepare memory for initialization.
	memset(&driverInfo, 0, sizeof(driverInfo));
	// Init TPM.
	TPM_DRV_Init(BOARD_TPM0_INSTANCE, &driverInfo);
	// Set clock for TPM.
	TPM_DRV_SetClock(BOARD_TPM0_INSTANCE, kTpmClockSourceModuleHighFreq, kTpmDividedBy1);

	TPM0Clock = TPM_DRV_GetClock(BOARD_TPM0_INSTANCE);
	CoreClock = CLOCK_SYS_GetSystemClockFreq();
	printf("TPM configurado\r\n");
	//printf("Core Clock: %d || TPM 0 Clock: %d || TPM Mod: %d \r\n", CoreClock, TPM0Clock, TPM_MAX_MOD-1);


}

/*!
 * @brief Write OF count values to release
 *
 *
 */
void writeOF(int OFMedido)
{
	char cOverflow[10];
	int i = 0, resto, div;
	div = OFMedido;
	for(i = 9; i >= 0; i--)
	{
		cOverflow[i] = div % 10 + '0';
		div =  div/10;
	}

	lcd_sendCommand(CMD_CLEAR);
	lcd_writeString(cOverflow);
	lcd_setCursor(1,0);

}
/*!
 * @brief Use FTM in PWM mode
 *
 * This function use PWM to control brightness of a LED.
 * LED is brighter and then dimmer, continuously.
 */
int main(void)
{
	rtc_datetime_t date, date_begin;
	init_board();
	LED1_EN;
	LED2_EN;
	LED2_ON;
    // Init RTC
    RTC_DRV_Init(RTC_INSTANCE);
    // Set a start date time and start RT.
    date.year   = 2016U;
    date.month  = 7U;
    date.day    = 31U;
    date.hour   = 16U;
    date.minute = 20U;
    date.second = 0U;

    date_begin = date;

    // Set RTC time to default
    RTC_DRV_SetDatetime(RTC_INSTANCE, &date);
	TPM_DRV_CounterStart(BOARD_TPM0_INSTANCE, kTpmCountingUp, TPM_MAX_MOD-1, 0);
	// Benchmark inicio
	  double  a1 = 1.0, b1 = -10.5, c1 = 32.0, d1 = -30.0;
	  double  a2 = 1.0, b2 = -4.5, c2 = 17.0, d2 = -30.0;
	  double  a3 = 1.0, b3 = -3.5, c3 = 22.0, d3 = -31.0;
	  double  a4 = 1.0, b4 = -13.7, c4 = 1.0, d4 = -35.0;
	  double  x[3];
	  double X;
	  int solutions;
	  int i;
	  unsigned long l = 0x3fed0169L;
	  struct int_sqrt q;
	  long n = 0;

	  /* solve soem cubic functions */
	  printf("********* CUBIC FUNCTIONS ***********\n");
	  /* should get 3 solutions: 2, 6 & 2.5   */
	  SolveCubic(a1, b1, c1, d1, &solutions, x);
	  printf("Solutions:");
	  for(i=0;i<solutions;i++)
	    printf(" %f",x[i]);
	  printf("\n");
	  /* should get 1 solution: 2.5           */
	  SolveCubic(a2, b2, c2, d2, &solutions, x);
	  printf("Solutions:");
	  for(i=0;i<solutions;i++)
	    printf(" %f",x[i]);
	  printf("\n");
	  SolveCubic(a3, b3, c3, d3, &solutions, x);
	  printf("Solutions:");
	  for(i=0;i<solutions;i++)
	    printf(" %f",x[i]);
	  printf("\n");
	  SolveCubic(a4, b4, c4, d4, &solutions, x);
	  printf("Solutions:");
	  for(i=0;i<solutions;i++)
	    printf(" %f",x[i]);
	  printf("\n");
	  /* Now solve some random equations */
	  for(a1=1;a1<10;a1++) {
	    for(b1=10;b1>0;b1--) {
	      for(c1=5;c1<15;c1+=0.5) {
		for(d1=-1;d1>-11;d1--) {
		  SolveCubic(a1, b1, c1, d1, &solutions, x);
		  printf("Solutions:");
		  for(i=0;i<solutions;i++)
		    printf(" %f",x[i]);
		  printf("\n");
		}
	      }
	    }
	  }

	  printf("********* INTEGER SQR ROOTS ***********\n");
	  /* perform some integer square roots */
	  for (i = 0; i < 1001; ++i)
	    {
	      usqrt(i, &q);
				// remainder differs on some machines
	     //printf("sqrt(%3d) = %2d, remainder = %2d\n",
	     printf("sqrt(%3d) = %2d\n", i, q.sqrt);
	    }
	  usqrt(l, &q);
	  //printf("\nsqrt(%lX) = %X, remainder = %X\n", l, q.sqrt, q.frac);
	  printf("\nsqrt(%lX) = %X\n", l, q.sqrt);


	  printf("********* ANGLE CONVERSION ***********\n");
	  /* convert some rads to degrees */
	  for (X = 0.0; X <= 360.0; X += 1.0)
	    printf("%3.0f degrees = %.12f radians\n", X, deg2rad(X));
	  puts("");
	  for (X = 0.0; X <= (2 * PI + 1e-6); X += (PI / 180))
	    printf("%.12f radians = %3.0f degrees\n", X, rad2deg(X));

	// Benchmark fim
	LED2_OFF;
	LED1_ON;
	int OFMedido = OF_COUNT;
	printf("Overflows: %d \r\n", OFMedido);
    RTC_DRV_GetDatetime(RTC_INSTANCE, &date);
    printf("Current datetime: %04hd-%02hd-%02hd %02hd:%02hd:%02hd\r\n",
            		date.year, date.month, date.day, date.hour, date.minute, date.second);
    date_begin.year =  date.year - date_begin.year;
    date_begin.month =  date.month - date_begin.month;
    date_begin.day =  date.day - date_begin.day;
    date_begin.hour =  date.hour - date_begin.hour;
    date_begin.minute =  date.minute - date_begin.minute;
    date_begin.second =  date.second - date_begin.second;

    printf("Current datetime: %04hd-%02hd-%02hd %02hd:%02hd:%02hd\r\n",
    		date_begin.year, date_begin.month, date_begin.day, date_begin.hour, date_begin.minute, date_begin.second);

    //Write results to LCD
    //writeOF(OFMedido);
}
///////////////////////////////////////////////////////////////////////////////
// EOF
///////////////////////////////////////////////////////////////////////////////

